## Build tools to simplify builds of react apps for FiveM

1. To simply add a build of create-react-app in a FiveM resource, include the `build-tools/` folder in the root of your project.

2. Modify the `build-tools/templates/template-resource.lua` to match your desired project, and include any .lua or .net.dll files, description and nuances in there.

3. Rename the `nui://` call in the `.env` file

4. Change `"build": "react-scripts build",` to `"build": "react-scripts build&&node build-tools/build-resources.js",`