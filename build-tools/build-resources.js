const fs = require("fs")
const path = require("path")

const buildDir = path.normalize('build/')
const htmlDir = path.join(buildDir, 'html')
const resourceTemplate = path.join(__dirname, 'templates/template-resource.lua')
const resourceOutput = path.join(buildDir, '__resource.lua')

const getFileNames = (dirPath, arrayOfFiles = []) => {
  const files = fs.readdirSync(dirPath)

  files.forEach(file => {
    const filePath = path.normalize(`${dirPath}/${file}`)

    if (fs.statSync(filePath).isDirectory()) {
      // The entry is a directory, get the files within
      arrayOfFiles = getFileNames(filePath, arrayOfFiles)
    } else {
      // Split the current path so reassemble with forward slash
      const pathArray = filePath.replace(buildDir, '').split(path.sep)
      arrayOfFiles.push(pathArray.join('/'))
    }
  })

  return arrayOfFiles
}

const moveBuildToHtmlDirectory = (dirPath) => {
  return new Promise((resolve, reject) => {
    const files = fs.readdirSync(dirPath)
    fs.mkdirSync(path.join(dirPath, 'html'))

    files.forEach(file => {
      fs.renameSync(path.join(dirPath, file), path.join(dirPath, 'html', file))
    })
    resolve()
  })
}

const generateResourceFile = () => {
  // Copy the resource file from template
  fs.copyFileSync(resourceTemplate, resourceOutput)

  // Get a list of the files to include
  const fileNames = getFileNames(htmlDir)

  // Fix the formatting for the resource file
  const fixedFormat = JSON.stringify(fileNames)
    .replace("[", "files {\n\t")
    .replace("]", "\n}")
    .replace(/,/g, ",\n\t")

    fs.appendFile(resourceOutput, fixedFormat, e => {
      if (e) throw e
      console.log('Saved file list!')
    })
}

moveBuildToHtmlDirectory(buildDir).then(() => generateResourceFile())